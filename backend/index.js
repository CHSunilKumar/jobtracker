const Joi = require("joi");
Joi.objectId = require("joi-objectid")(Joi);
const mongoose = require("mongoose");
const users = require("./routes/users");
const auth = require("./routes/auth");
const addUser = require("./routes/addUser");
const express = require("express");
const cors = require("cors");
const app = express();

mongoose
  .connect("mongodb://localhost/EmployeeDB")
  .then(() => console.log("Now connected to MongoDB!"))
  .catch((err) => console.error("Something went wrong", err));

app.use(express.json());
app.use(cors());
app.use("/api/users", users);
app.use("/api/auth", auth);
app.use("/api/test", require("./routes/userch"));
app.use("/api/authorization", require("./routes/authorization"));
app.use("/api/dashboard", require("./routes/dashboard"));
app.use("/api/addUser", require("./routes/addUser"));
app.use("/api/getuser", require("./routes/fetchUser"));
app.use("/api/fetchUser", require("./routes/fetchUser"));
app.use("/api/adddept", require("./routes/Adddepartment"));
app.use("/forgotpassword", require("./routes/forgotpassword"));

const port = process.env.PORT || 4000;
app.listen(port, () => console.log(`Listening on port ${port}...`));
