const Joi = require("@hapi/joi");
const bcrypt = require("bcrypt");
const _ = require("lodash");
const User = require("../models/schemauser");
const express = require("express");
const router = express.Router();

router.post("/", async (req, res) => {
  // First Validate The HTTP Request
  // const { error } = validate(req.body);
  // if (error) {
  //     return res.status(400).send(error.details[0].message);
  // }

  try {
    const user = await User.findOne({ email: req.body.email });
    console.log(req.body.email);
    console.log(user);
    if (!user) {
      return res.status(400).send("Incorrect email or password.");
    }

    // Then validate the Credentials in MongoDB match
    // those provided in the request
    const validPassword = await bcrypt.compare(
      req.body.password,
      user.password
    );
    if (!validPassword) {
      return res.status(400).send("Incorrect email or password.");
    }

    console.log(user.role);

    if (user.role == "admin") {
      res.send("Welcome Admin");
    } else if (user.role == "superadmin") {
      res.send("Welcome Superadmin");
    } else {
      res.send("Welcome User");
    }

    // res.send("Login Successfully Created!");
  } catch (error) {
    res.send(error);
  }

  //  Now find the user by their email address

  // res.json({
  //     message:"Login Success!"
  // });
});

router.get("/", async (req, res) => {
  try {
    const body = await User.find();
    res.send(body);
  } catch (error) {
    res.send(error);
  }
});

// function validate(req) {
//     const schema = {
//         email: Joi.string().min(5).max(255).required().email(),
//         password: Joi.string().min(5).max(255).required()
//     };

//     return Joi.validate(req, schema);
// }

module.exports = router;
