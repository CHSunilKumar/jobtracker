const { json } = require("body-parser");
const express = require("express");
const routes = express.Router();
const JSON = require("JSON");
const pr = require("../models/process");

routes.get("/count", async (req, res) => {
  try {
    const completed = await pr.find({ Status: "Completed" });
    const overdue = await pr.find({ Status: "Overdue" });
    const due = await pr.find({ Status: "Due" });
    const inprogress = await pr.find({ Status: "InProgress" });
    // console.log("Done count " + l.length);

    //res.send(l)
    pr.aggregate([
      { $group: { _id: null, count: { $sum: 1 } } },
      { $project: { _id: 0 } },
    ]).exec(function (err, results) {
      if (err) throw err;
      // console.log(results);
      // console.log(
      //   "Total Records " +
      //     results.length +
      //     "\n Done count " +
      //     l.length +
      //     "\n Not Completed " +
      //     lm.length
      // );

      json(
        "Completed:" +
          completed.length +
          "  Overdue:" +
          overdue.length +
          "  Due:" +
          due.length +
          "  Inprogress:" +
          inprogress.length
      );

      res.json(
        // "Total Records " +
        //   results.length +

        "Completed:" +
          completed.length +
          "  Overdue:" +
          overdue.length +
          "  Due:" +
          due.length +
          "  Inprogress:" +
          inprogress.length
      );
    });
  } catch (error) {
    res.send("Error " + error);
  }
});

module.exports = routes;
