const express = require("express");
const routes = express.Router();
const dept = require("../models/department");
const mongoose = require("mongoose");
// const emp = require("../models/Register");

routes.post("/", async (req, res) => {
  const body = new dept({
    Department_Name: req.body.Department_Name,
    Department_Members: req.body.Department_Members,
    Department_Supervisor: req.body.Department_Supervisor,
  });
  try {
    await body.save();
    res.send(body);
  } catch (error) {
    res.send("Error" + error);
  }
});
routes.get("/emp", async (req, res) => {
  try {
    const b = await emp.find({}, { Username: 1, _id: 0 });
    res.json(b);
  } catch (error) {
    res.send("Error" + error);
  }
});
routes.get("/", async (req, res) => {
  try {
    const body = await dept.find();

    res.json(body);
  } catch (error) {
    res.send("Error" + error);
  }
});
routes.get("/count", async (req, res) => {
  try {
    dept
      .aggregate([
        {
          $group: { _id: null, count: { $sum: 1 } },
        },
        { $project: { _id: 0 } },
      ])
      .exec(function (err, results) {
        if (err) throw err;
        console.log(results);
        //console.log(results[2]['Combined_List']['list'])
        res.send(results);
      });
  } catch (error) {
    res.send("Error " + error);
  }
});

routes.patch("/", async (req, res) => {
  try {
    const body = await dept.findOneAndUpdate(
      { Department_Name: req.body.Department_Name },
      { Department_Members: req.body.Department_Members }
    );
    res.send("Members Updated");
  } catch (error) {
    res.send("Error " + error);
  }
});
module.exports = routes;
