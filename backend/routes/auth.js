const Joi = require("@hapi/joi");
const bcrypt = require("bcrypt");
const _ = require("lodash");
const { User } = require("../models/user");
const express = require("express");
const router = express.Router();

router.post("/", async (req, res) => {
  // First Validate The HTTP Request
  // const { error } = validate(req.body);
  console.log(req.body.email);
  //   if (error) {
  //     return res.status(400).send(error.details[0].message);
  //   }

  //  Now find the user by their email address
  let user = await User.find({ email: req.body.email });
  console.log(user[0]["password"]);
  //   if (!user) {
  //     return res.status(400).send("Incorrect email or password.");
  //   }

  // Then validate the Credentials in MongoDB match
  // those provided in the request
  // var validPassword = await bcrypt.compare(req.body.password, user.password);
  const salt = await bcrypt.genSalt(10);
  const pass = await bcrypt.hash(req.body.password, salt);
  console.log(pass);

  console.log(bcrypt.compare(req.body.password, user[0]["password"]));
  if (req.body.password == user[0]["confirmpassword"]) {
    res.send("Login Successfully Created!");
  } else {
    res.send("Incorrect email or password.");
  }

  // res.json({
  //     message:"Login Success!"
  // });
});

router.get("/", async (req, res) => {
  const body = await User.find();
  res.send(body);
});

function validate(req) {
  const schema = {
    email: Joi.string().min(5).max(255).required().email(),
    password: Joi.string().min(5).max(255).required(),
  };

  return Joi.validate(req, schema);
}

module.exports = router;
