const Joi = require('@hapi/joi');
const mongoose = require('mongoose');

const User = mongoose.model('User', new mongoose.Schema({
    firstname: {
        type: String,
        // required: true,
        minlength: 5,
        maxlength: 50
    },
    lastname: {
        type: String,
        // required: "email is required",
        minlength: 5,
        maxlength: 50
    },
    email: {
        type: String,
        // required: "email is required",
        minlength: 5,
        maxlength: 255,
        unique: true
    },
    phonenumber: {
        type: Number,
        // required: true,
        minlength: 10,
        //maxlength: 10,
        unique: true
    },
    password: {
        type: String,
        // required: true,
        minlength: 5,
        maxlength: 1024
    },
    confirmpassword: {
        type: String,
        // required: true,
        minlength: 5,
        maxlength: 1024
    },
    role: {
        type: String

    }
}));

function validateUser(user) {
    const schema = {
        firstname: Joi.string().min(5).max(50).required(),
        lastname: Joi.string().min(5).max(50).required(),
        email: Joi.string().min(5).max(255).required().email(),
        // phonenumber:Joi.string().length(10).pattern(/^[0-9]+$/).required(),
        phonenumber: Joi.number().min(10).required(),
        password: Joi.string().min(5).max(255).required(),
        confirmpassword: Joi.string().valid(Joi.ref('password')).required(),
        role: Joi.string().required(),
    };
    return Joi.validate(user, schema);
}

exports.User = User;
exports.validate = validateUser;