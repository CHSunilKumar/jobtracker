const mongoose = require('mongoose')
const autoIncrement = require("mongoose-auto-increment");
const departments = new mongoose.Schema({
    Department_id:{
        type:Number,
        required:true
    },
    Department_Name:{
        type:String,
        required:true,
        unique:true
    },
    Department_Members:[{

    }],
    Department_Supervisor:{
        type:String,
        required:true,
        unique:true
    }
})
autoIncrement.initialize(mongoose.connection);
departments.plugin(autoIncrement.plugin, {
  model: "post", // collection or table name in which you want to apply auto increment
  field: "Department_id", // field of model which you want to auto increment
  startAt: 1, // start your auto increment value from 1
  incrementBy: 1, // incremented by 1
});
module.exports = mongoose.model('Departments',departments)