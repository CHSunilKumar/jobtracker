export const registerURL = "http://localhost:4000/api/users";
export const loginURL = "http://localhost:4000/api/auth";
export const forgotPasswordURL = "http://localhost:4000/forgotpassword";
export const dashboardURL = "http://localhost:4000/api/dashboard/count";
export const addemployeeURL = "http://localhost:4000/api/addUser";
export const viewemployeeURL =
  "http://localhost:4000/api/fetchUser/fetchemployee";
export const adddepartmentURL = "http://localhost:4000/api/adddept";
export const getuserURL = "http://localhost:4000/api/getuser/firstname";
