import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Form, Image, Navbar, Nav, Row, Col } from "react-bootstrap";
import logo from "../images/Maber.png";
import process from "../images/Process.png";
import employees from "../images/Employees.png";
import employeesselected from "../images/EmployeesSelected.png";
import department from "../images/Department.png";
import processcreation from "../images/ProcessCreation.png";
import addbuttonactive from "../images/AddButtonActive.png";
import employeephoto from "../images/AddPhoto.png";
import { notification } from "antd";
import { connect } from "react-redux";
import { addemployeeAction } from "../actions/addemployeeAction";
import SimpleReactValidator from "simple-react-validator";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

export class AddEmployee extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstname: "",
      lastname: "",
      employeecode: "",
      department: "",
      supervisor: "",
      designation: "",
      selectedrole: "",
      officetel: "",
      mobile: "",
      email: "",
      officename: "",
    };
    this.changeData = this.changeData.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.validator = new SimpleReactValidator();
  }

  changeData(type, event) {
    switch (type) {
      case "firstname":
        this.setState({ firstname: event.target.value });
        break;
      case "lastname":
        this.setState({ lastname: event.target.value });
        break;
      case "employeecode":
        this.setState({ employeecode: event.target.value });
        break;
      case "department":
        this.setState({ department: event.target.value });
        break;
      case "supervisor":
        this.setState({ supervisor: event.target.value });
        break;
      case "designation":
        this.setState({ designation: event.target.value });
        break;
      case "officetel":
        this.setState({ officetel: event.target.value });
        break;
      case "mobile":
        this.setState({ mobile: event.target.value });
        break;
      case "email":
        this.setState({ email: event.target.value });
        break;
      case "officename":
        this.setState({ officename: event.target.value });
        break;
      default:
        break;
    }
  }

  componentWillReceiveProps(postProps) {
    if (
      !Object.is(postProps.addemployeeReducer, this.props.addemployeeReducer)
    ) {
      if (postProps.addemployeeReducer.data.Error) {
        notification.open({
          message: "alert",
          description: postProps.addemployeeReducer.data.data.error.message,
        });
      }
      if (postProps.addemployeeReducer.data.Loading) {
        this.setState({ loading: true });
      } else {
        this.setState({ loading: false });
      }
    }
    // console.log("Hello", postProps.addemployeeReducer);
    if (
      !postProps.addemployeeReducer.data.Loading &&
      !postProps.addemployeeReducer.data.Error
    ) {
      toast.success("LogIn Sucessfully!!!", {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
      // notification.open({
      //   message: "alert",
      //   description: "You are successfully Signed Up",
      // });
      setTimeout(() => {
        this.props.history.push({ pathname: "/employee" });
      }, 5000);
    }
  }

  handleChange(e) {
    this.setState({
      selectedrole: e.target.value,
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    if (this.validator.allValid()) {
      var self = this;
      toast.success("Employee Added Sucessfully!!!", {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
      // notification.open({
      //   message: "alert",
      //   description: "You are successfully Signed Up",
      // });
      setTimeout(() => {
        this.props.history.push({ pathname: "/employee" });
      }, 5000);
      this.props.addemployeeAction(
        this.state.firstname,
        this.state.lastname,
        this.state.employeecode,
        this.state.department,
        this.state.supervisor,
        this.state.designation,
        this.state.selectedrole,
        this.state.officetel,
        this.state.mobile,
        this.state.email,
        this.state.officename
      );
    } else {
      this.validator.showMessages();
      this.forceUpdate();
    }
  }

  render() {
    return (
      <Form>
        <div className="site-card-wrapper">
          <div>
            <Navbar
              collapseOnSelect
              expand="lg"
              className="navbar navbar-expand-md navbar-light bg-light"
            >
              <div>
                <Image
                  className="processimage"
                  style={{ margin: "50%" }}
                  src={process}
                />
              </div>

              <div className="navbar-brand">
                <div className="col-auto">
                  <Image style={{ marginLeft: "20%" }} src={logo} />
                </div>
              </div>

              <Navbar.Toggle aria-controls="responsive-navbar-nav" />
              <Navbar.Collapse id="responsive-navbar-nav">
                <div className="navbar-nav ml-auto">
                  <div className="navbar-brand mr-auto">
                    <Nav className="topnav-centered">
                      <label className="topnav-centered">
                        PROCESS CREATION BOARD
                      </label>
                    </Nav>
                  </div>

                  <div className="navbar-brand">
                    <button type="button" className="btn_Create_Account">
                      CH Sunil Kumar
                    </button>
                  </div>
                </div>
              </Navbar.Collapse>
            </Navbar>
          </div>

          <div className="sasidebar" style={{ width: "20%" }}>
            <Link to={"./addmilestone"} style={{ marginTop: "5%" }}>
              <Image
                className="employee"
                style={{ marginLeft: "30%" }}
                src={employees}
              />
              Processes
            </Link>
            <Link
              className="active"
              to={"./addemployee"}
              style={{ marginTop: "5%" }}
            >
              <Image
                className="employee"
                style={{ marginLeft: "30%" }}
                src={employeesselected}
              />
              Employees
            </Link>
            <Link to={"./viewdepartments"} style={{ marginTop: "5%" }}>
              <Image
                className="department"
                style={{ marginLeft: "30%" }}
                src={department}
              />
              Departments
            </Link>
            <div>
              <Image className="processimg" src={processcreation} />
            </div>
          </div>

          <div className="container">
            <div className="input-group">
              <label
                className="dptlabel"
                style={{
                  marginLeft: "20%",
                  marginTop: "2%",
                }}
              >
                EMPLOYEES
              </label>
            </div>

            <div className="col-auto">
              <input
                type="text"
                className="form-control"
                style={{
                  width: "40%",
                  borderRadius: "25px",
                  color: "gray",
                  marginLeft: "20%",
                  marginTop: "1%",
                }}
                placeholder="Filter By Name"
              />

              <Image
                style={{ height: "20px", width: "20px", marginLeft: "2%" }}
                src={addbuttonactive}
              />

              <label style={{ color: "gray", marginLeft: "1%" }}>
                Add Employee
              </label>
            </div>

            <div className="col-auto borders">
              <div className="input-group">
                <label className="dptlabel" style={{ marginTop: "0%" }}>
                  Add Employee
                </label>
              </div>

              {/* <div className="col-auto container">
                <div className="input-group-prepend">
                  <Image src={employeephoto} className="profile" />
                </div>
              </div>
              <div className="col-auto container">
                <div className="input-group-prepend">
                  <label
                    style={{
                      color: "gray",
                      marginLeft: "20%",
                      marginTop: "1%",
                    }}
                  >
                    Add Photo
                  </label>
                </div>
              </div> */}

              <div className="col-auto container">
                <Row>
                  <Col>
                    <input
                      type="text"
                      className="form-control"
                      placeholder="First Name"
                      onChange={this.changeData.bind(this, "firstname")}
                    />
                    {this.validator.message(
                      "First Name",
                      this.state.firstname,
                      "required|alpha|min:3|max:20",
                      "text-danger"
                    )}
                  </Col>

                  <Col>
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Last Name"
                      onChange={this.changeData.bind(this, "lastname")}
                    />
                    {this.validator.message(
                      "Last Name",
                      this.state.lastname,
                      "required|alpha|min:3|max:20",
                      "text-danger"
                    )}
                  </Col>

                  <Col>
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Employee Code"
                      onChange={this.changeData.bind(this, "employeecode")}
                    />
                    {this.validator.message(
                      "Employee Code",
                      this.state.employeecode,
                      "required|alpha_num|min:3|max:20",
                      "text-danger"
                    )}
                  </Col>
                </Row>
              </div>

              <div className="col-auto container">
                <Row>
                  <Col>
                    <input
                      style={{ marginTop: "0%" }}
                      type="text"
                      className="form-control"
                      placeholder="Department"
                      onChange={this.changeData.bind(this, "department")}
                    />
                    {this.validator.message(
                      "Department",
                      this.state.department,
                      "required|alpha|min:3|max:20",
                      "text-danger"
                    )}
                  </Col>
                  <Col>
                    <input
                      style={{ marginTop: "0%" }}
                      type="text"
                      className="form-control"
                      placeholder="Supervisor"
                      onChange={this.changeData.bind(this, "supervisor")}
                    />
                    {this.validator.message(
                      "Supervisor",
                      this.state.supervisor,
                      "required|alpha|min:3|max:20",
                      "text-danger"
                    )}
                  </Col>
                </Row>
              </div>

              <div className="col-auto container">
                <Row>
                  <Col>
                    <input
                      style={{ marginTop: "0%" }}
                      type="text"
                      className="form-control"
                      placeholder="Designation"
                      onChange={this.changeData.bind(this, "designation")}
                    />
                    {this.validator.message(
                      "Designation",
                      this.state.designation,
                      "required|alpha|min:3|max:20",
                      "text-danger"
                    )}
                  </Col>
                  <Col>
                    <select
                      className="ddl dptddl"
                      style={{
                        width: "100%",
                        marginTop: "0%",
                      }}
                      defaultValue={"Select"}
                      onChange={this.handleChange}
                    >
                      <option value="Select" disabled>
                        Select
                      </option>
                      <option value="Super Admin">Super Admin</option>
                      <option value="Admin">Admin</option>
                      <option value="User">User</option>
                    </select>
                    {this.validator.message(
                      "Role",
                      this.state.selectedrole,
                      "required",
                      "text-danger"
                    )}
                  </Col>
                </Row>
              </div>

              <div className="col-auto container">
                <Row>
                  <Col>
                    <input
                      style={{ marginTop: "0%" }}
                      type="tel"
                      className="form-control"
                      placeholder="Office Tel"
                      onChange={this.changeData.bind(this, "officetel")}
                    />
                  </Col>
                  <Col>
                    <input
                      style={{ marginTop: "0%" }}
                      type="tel"
                      className="form-control"
                      placeholder="Mobile Number"
                      onChange={this.changeData.bind(this, "mobile")}
                    />
                    {this.validator.message(
                      "Mobile Number",
                      this.state.mobile,
                      "required|numeric|min:10",
                      "text-danger"
                    )}
                  </Col>
                </Row>
              </div>

              <div className="col-auto container">
                <Row>
                  <Col>
                    <input
                      style={{ width: "48%", marginTop: "0%" }}
                      type="email"
                      className="form-control"
                      placeholder="Email"
                      onChange={this.changeData.bind(this, "email")}
                    />
                    {this.validator.message(
                      "Email",
                      this.state.email,
                      "required|email",
                      "text-danger"
                    )}
                  </Col>
                </Row>
              </div>

              <div className="col-auto container">
                <Row>
                  <Col>
                    <input
                      style={{ width: "48%", marginTop: "0%" }}
                      type="text"
                      className="form-control"
                      placeholder="Office"
                      onChange={this.changeData.bind(this, "officename")}
                    />
                    {this.validator.message(
                      "Office",
                      this.state.officename,
                      "required|alpha|min:3|max:20",
                      "text-danger"
                    )}
                  </Col>
                </Row>
              </div>

              <div className="container">
                <Row>
                  <div className="col-md-12  text-right">
                    <Link
                      to={"./dashboard"}
                      style={{ marginRight: "8%" }}
                      className="Cancel"
                    >
                      Cancel
                    </Link>
                    <button
                      type="submit"
                      style={{
                        // marginRight: "12%",
                        borderRadius: "10px",
                      }}
                      className="btnNext ml-2"
                      onClick={this.handleSubmit}
                    >
                      ADD
                      <ToastContainer />
                    </button>
                  </div>
                </Row>
              </div>
            </div>
          </div>
        </div>
      </Form>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    addemployeeAction: (
      firstname,
      lastname,
      employeecode,
      department,
      supervisor,
      designation,
      selectedrole,
      officetel,
      mobile,
      email,
      officename
    ) => {
      dispatch(
        addemployeeAction(
          firstname,
          lastname,
          employeecode,
          department,
          supervisor,
          designation,
          selectedrole,
          officetel,
          mobile,
          email,
          officename
        )
      );
    },
  };
};

const mapStateToProps = (state) => {
  return { addemployeeReducer: state.addemployeeReducer };
};

export default connect(mapStateToProps, mapDispatchToProps)(AddEmployee);
