import React, { Component } from "react";
import { Card, Col, Form, Row, Image } from "react-bootstrap";
import { Link } from "react-router-dom";
import Maberlogo from "../images/Maberlogo.png";
import login from "../images/login.png";
import SimpleReactValidator from "simple-react-validator";

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
    };
    this.changeData = this.changeData.bind(this);
    this.handlenext = this.handlenext.bind(this);
    this.validator = new SimpleReactValidator();
  }
  changeData(type, event) {
    if (type === "email") {
      this.setState({ email: event.target.value });
    }
  }

  handlenext(event) {
    event.preventDefault();

    if (this.validator.allValid()) {
      var self = this;
      this.props.history.push({
        pathname: "/password",
        state: { email: this.state.email },
      });
    } else {
      this.validator.showMessages();
      this.forceUpdate();
    }
  }

  render() {
    return (
      <Form>
        <div className="site-card-wrapper container">
          <Row>
            <Col className="col-xs-7 col-centered">
              <Card className="card border-0">
                <div className="col-auto container">
                  <Image className="imglogin" src={login} />
                </div>

                <div className="col-auto container">
                  <Image
                    style={{ width: "50%", marginLeft: "20%" }}
                    src={Maberlogo}
                  />
                </div>

                <div className="col-auto container">
                  <label
                    className="label_Create_Account"
                    style={{ marginRight: "40px" }}
                  >
                    Sign In
                  </label>
                </div>

                <div className="col-auto container">
                  <label
                    style={{ marginRight: "40px", color: "rgba(0, 0, 0, 1)" }}
                    className="lbl"
                  >
                    to continue to MABER
                  </label>
                </div>

                <div className="col-auto container">
                  <div className=" input-group-prepend">
                    <input
                      style={{ width: "600px" }}
                      type="email"
                      className="form-control"
                      placeholder="Email Address"
                      onChange={this.changeData.bind(this, "email")}
                    />
                  </div>
                  {this.validator.message(
                    "Email",
                    this.state.email,
                    "required|email",
                    "text-danger"
                  )}
                </div>

                <div className="col-auto container">
                  <Link
                    to={"./forgotpassword"}
                    style={{ marginRight: "20em" }}
                    className="Forgot_Email"
                  >
                    Forgot Email?
                  </Link>
                </div>

                <div className="col-auto container">
                  <Link
                    to={"./register"}
                    style={{ marginRight: "20em" }}
                    className="Create_Account"
                  >
                    Create Account
                  </Link>
                </div>

                <div className="col-auto container">
                  {/* <Link to={"./Password"} className="btnNext">
                    Next
                  </Link> */}
                  <button
                    type="submit"
                    className="btnNext"
                    onClick={this.handlenext}
                  >
                    Next
                  </button>
                </div>
              </Card>
            </Col>
          </Row>
        </div>
      </Form>
    );
  }
}
