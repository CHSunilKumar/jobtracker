import React, { Component } from "react";
import { Link } from "react-router-dom";
import Maberlogo from "../images/Maberlogo.png";
import SimpleReactValidator from "simple-react-validator";
import { forgotpasswordAction } from "../actions/forgotpasswordAction";
import { connect } from "react-redux";
import { Image, Form, Row, Col, Card } from "react-bootstrap";
import { notification } from "antd";

export class ForgotPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Email: "",
    };
    this.changeData = this.changeData.bind(this);
    this.validator = new SimpleReactValidator();
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  changeData(type, event) {
    if (type === "Email") {
      this.setState({ Email: event.target.value });
    }
  }

  handleSubmit(event) {
    event.preventDefault();

    if (this.validator.allValid()) {
      var self = this;
      /* notification.open({
        message: "alert",
        description: "Email Sent!",
      });  */
      this.props.forgotpasswordAction(this.state.Email);
    } else {
      this.validator.showMessages();
      this.forceUpdate();
    }
  }

  render() {
    return (
      <Form>
        <div className="site-card-wrapper container">
          <Row>
            <Col className="col-sm-12">
              <Card className="card border-0">
                <div className="col-auto container">
                  <Image
                    style={{
                      width: "50%",
                      marginLeft: "20%",
                      marginTop: "100%",
                    }}
                    src={Maberlogo}
                  />
                </div>
                <div className="col-auto container">
                  <label
                    className="label_Create_Account"
                    style={{ fontSize: "35px", marginTop: "20px" }}
                  >
                    Forgot Password?
                  </label>
                </div>

                <div className="col-auto container">
                  <label
                    className="label_Create_Account"
                    style={{ marginTop: "10px" }}
                  >
                    Enter Registered Email
                  </label>
                </div>

                <div className="col-auto container">
                  <div className="input-group-prepend">
                    <input
                      style={{ width: "600px" }}
                      type="email"
                      placeholder="Email Address"
                      className="form-control"
                      onChange={this.changeData.bind(this, "Email")}
                    />
                  </div>
                  {this.validator.message(
                    "Email",
                    this.state.Email,
                    "required|email",
                    "text-danger"
                  )}
                </div>

                <div className="col-auto container">
                  <div className="input-group-prepend">
                    <label
                      className="label_Create_Account"
                      style={{ marginRight: "17em" }}
                    >
                      Send Reset Link
                    </label>
                  </div>
                </div>

                <div className="col-auto container">
                  <div className="input-group-prepend">
                    <Link
                      to={"./login"}
                      className="Cancel"
                      style={{ marginRight: "23em" }}
                    >
                      Cancel
                    </Link>
                  </div>
                </div>

                <div className="col-auto container">
                  <div className="input-group-prepend">
                    <button
                      type="submit"
                      className="btnNext"
                      onClick={this.handleSubmit}
                    >
                      Submit
                    </button>
                  </div>
                </div>
              </Card>
            </Col>
          </Row>
        </div>
      </Form>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    forgotpasswordAction: (Email) => {
      dispatch(forgotpasswordAction(Email));
    },
  };
};

const mapStateToProps = (state) => {
  return { forgotpasswordReducer: state.forgotpasswordReducer };
};

export default connect(mapStateToProps, mapDispatchToProps)(ForgotPassword);
// export default ForgotPassword;
