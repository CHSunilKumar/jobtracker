import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Form, Image, Navbar, Nav, Row, Col } from "react-bootstrap";
import logo from "../images/Maber.png";
import process from "../images/Process.png";
import employees from "../images/Employees.png";
import departmentselected from "../images/DepartmentSelected.png";
import processcreation from "../images/ProcessCreation.png";
import addbuttonactive from "../images/AddButtonActive.png";
import SimpleReactValidator from "simple-react-validator";
import { adddepartmentAction } from "../actions/adddepartmentAction";
import { connect } from "react-redux";
import { Card, notification } from "antd";
import { getuserAction } from "../actions/getuserAction";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

export class AddDepartment extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Department_id: "",
      Department_Name: "",
      Department_Members: [],
      Department_Supervisor: "",
      getuserNameOption: [],
      loading: false,
      signUpError: "",
      signUpDisplay: false,
    };
    this.changeData = this.changeData.bind(this);
    this.validator = new SimpleReactValidator();
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  componentWillReceiveProps(postProps) {
    let getuserName = [];
    let getuserTable = [];

    if (!Object.is(postProps.getuserReducer, this.props.getuserReducer)) {
      if (postProps.getuserReducer.data.Error) {
        notification.open({
          message: "alert",
          description: "Some internal error !!!",
        });
      }
      if (postProps.getuserReducer.data.Loading) {
        this.setState({ loadingGetuser: true });
      } else {
        this.setState({ loadingGetuser: false });
      }
      console.log("hai", postProps.getuserReducer);
      if (
        postProps.getuserReducer.data.data &&
        postProps.getuserReducer.data.data.response &&
        postProps.getuserReducer.data.data.response.data
      ) {
        getuserName = [];
        console.log("maber", postProps.getuserReducer.data.data.response.data);
        let getuserNameOption = [];
        let getuserObject = postProps.getuserReducer.data.data.response.data;
        Object.entries(getuserObject).forEach(([_id, firstname]) => {
          getuserTable.push({
            title: firstname,
            dataIndex: _id,
            key: _id,
          });

          getuserName.push({ label: firstname, key: _id });

          getuserNameOption.push(
            <option value={firstname["firstname"]}>
              {firstname["firstname"]}
            </option>
          );
          console.log("jobtracker", firstname["_id"], firstname["firstname"]);
          this.setState({ getuserNameOption });
        });
      }
    }
  }

  changeData(type, event) {
    switch (type) {
      case "Department_id":
        this.setState({ Department_id: event.target.value });
        break;
      case "Department_Name":
        this.setState({ Department_Name: event.target.value });
        break;
      case "Department_Members":
        this.setState({ Department_Members: event.target.value });
        break;
      case "Department_Supervisor":
        this.setState({ Department_Supervisor: event.target.value });
        break;
      default:
        break;
    }
  }

  handleClick(event) {
    event.preventDefault();
    this.props.getuserAction(this.state._id, this.state.firstname);
  }

  handleSubmit(event) {
    event.preventDefault();
    if (this.validator.allValid()) {
      var self = this;
      // notification.open({
      //   message: "alert",
      //   description: "Department Added",
      // });
      toast.success("Department Added", {
        position: "top-right",
        autoClose: 10000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });

      this.props.adddepartmentAction(
        this.state.Department_id,
        this.state.Department_Name,
        this.state.Department_Members,
        this.state.Department_Supervisor
      );
    } else {
      this.validator.showMessages();
      this.forceUpdate();
    }
  }

  render() {
    return (
      <Form>
        <div className="site-card-wrapper">
          <div>
            <Navbar
              collapseOnSelect
              expand="lg"
              className="navbar navbar-expand-md navbar-light bg-light"
            >
              <div>
                <Image
                  className="processimage"
                  style={{ margin: "50%" }}
                  src={process}
                />
              </div>

              <div className="navbar-brand">
                <div className="col-auto">
                  <Image style={{ marginLeft: "20%" }} src={logo} />
                </div>
              </div>

              <Navbar.Toggle aria-controls="responsive-navbar-nav" />
              <Navbar.Collapse id="responsive-navbar-nav">
                <div className="navbar-nav ml-auto">
                  <div className="navbar-brand mr-auto">
                    <Nav className="topnav-centered">
                      <label className="topnav-centered">
                        PROCESS CREATION BOARD
                      </label>
                    </Nav>
                  </div>

                  <div className="navbar-brand">
                    <button type="button" className="btn_Create_Account">
                      CH Sunil Kumar
                    </button>
                  </div>
                </div>
              </Navbar.Collapse>
            </Navbar>
          </div>

          <div className="sasidebar" style={{ width: "20%" }}>
            <Link to={"./addmilestone"} style={{ marginTop: "5%" }}>
              <Image
                className="employee"
                style={{ marginLeft: "30%" }}
                src={employees}
              />
              Processes
            </Link>
            <Link to={"./addemployee"} style={{ marginTop: "5%" }}>
              <Image
                className="employee"
                style={{ marginLeft: "30%" }}
                src={employees}
              />
              Employees
            </Link>
            <Link
              className="active"
              to={"./viewdepartments"}
              style={{ marginTop: "5%" }}
            >
              <Image
                className="department"
                style={{ marginLeft: "30%" }}
                src={departmentselected}
              />
              Departments
            </Link>
            <div>
              <Image className="processimg" src={processcreation} />
            </div>
          </div>

          <div className="container">
            <div className="input-group">
              <label
                className="dptlabel"
                style={{
                  marginLeft: "20%",
                }}
              >
                DEPARTMENTS
              </label>
            </div>

            <div className="col-auto">
              <input
                type="text"
                className="form-control"
                style={{
                  width: "40%",
                  borderRadius: "25px",
                  color: "gray",
                  marginLeft: "20%",
                  marginTop: "1%",
                }}
                placeholder="Filter By Name"
              />

              <Image
                style={{ height: "20px", width: "20px", marginLeft: "2%" }}
                src={addbuttonactive}
              />

              <label style={{ color: "gray", marginLeft: "1%" }}>
                Add Department
              </label>
            </div>

            <div className="col-auto borders">
              <div className="input-group">
                <label className="dptlabel">Add Department</label>
              </div>
              <div className="col-auto container">
                <div className="input-group-prepend">
                  <input
                    type="text"
                    style={{
                      width: "48%",
                    }}
                    className="form-control"
                    placeholder="Department Number"
                    onChange={this.changeData.bind(this, "Department_id")}
                  />
                </div>
                {this.validator.message(
                  "Department_id",
                  this.state.Department_id,
                  "required",
                  "text-danger"
                )}
              </div>

              <div className="col-auto container">
                <Row>
                  <Col>
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Department Name"
                      name="name"
                      onChange={this.changeData.bind(this, "Department_Name")}
                    />
                    {this.validator.message(
                      "Department_Name",
                      this.state.Department_Name,
                      "required|alpha|min:3|max:20",
                      "text-danger"
                    )}
                  </Col>
                  <Col>
                    <select
                      className="ddl dptddl"
                      style={{
                        width: "100%",
                      }}
                      value={this.state.value}
                      onChange={this.changeData.bind(
                        this,
                        "Department_Supervisor"
                      )}
                      onClick={this.handleClick}
                      defaultValue={"Add Department Supervisor"}
                    >
                      <option value="Add Department Supervisor" disabled>
                        Add Department Supervisor
                      </option>
                      {/* <option disabled selected>
                        Select Department Supervisor
                      </option> */}
                      {this.state.getuserNameOption}
                    </select>
                    {this.validator.message(
                      "Department_Supervisor",
                      this.state.Department_Supervisor,
                      "required",
                      "text-danger"
                    )}
                  </Col>
                </Row>
              </div>

              <div className="col-auto container">
                <div className="input-group-prepend">
                  <select
                    className="ddl dptddl"
                    style={{
                      width: "48%",
                    }}
                    value={this.state.value}
                    onChange={this.changeData.bind(this, "Department_Members")}
                    onClick={this.handleClick}
                    defaultValue={"Add Department Members"}
                  >
                    <option value="Add Department Members" disabled>
                      Add Department Members
                    </option>
                    {/* <option disabled selected>
                      Select Department Members
                    </option> */}
                    {this.state.getuserNameOption}
                  </select>
                  {this.validator.message(
                    "Department_Members",
                    this.state.Department_Members,
                    "required",
                    "text-danger"
                  )}
                </div>
              </div>
              <div className="col-auto container">
                <div className="form-group">
                  <label></label>
                  <label></label>
                  <label></label>
                </div>
              </div>

              <div className="container">
                <Row>
                  <div className="col-md-12  text-right">
                    <Link
                      to={"./dashboard"}
                      style={{ marginRight: "8%" }}
                      className="Cancel"
                    >
                      Cancel
                    </Link>
                    <button
                      type="submit"
                      onClick={this.handleSubmit}
                      style={{
                        // marginRight: "12%",
                        borderRadius: "10px",
                      }}
                      className="btnNext ml-2"
                    >
                      ADD
                      <ToastContainer />
                    </button>
                  </div>
                </Row>
              </div>
            </div>
          </div>
        </div>
      </Form>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    adddepartmentAction: (
      Department_id,
      Department_Name,
      Department_Members,
      Department_Supervisor
    ) => {
      dispatch(
        adddepartmentAction(
          Department_id,
          Department_Name,
          Department_Members,
          Department_Supervisor
        )
      );
    },
    getuserAction: (_id, firstname) => {
      dispatch(getuserAction(_id, firstname));
    },
  };
};

const mapStateToProps = (state) => {
  return {
    adddepartmentReducer: state.adddepartmentReducer,
    getuserReducer: state.getuserReducer,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AddDepartment);
