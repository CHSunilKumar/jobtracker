import React, { Component } from "react";
import { connect } from "react-redux";
import { Card, Col, Form, Row, Image } from "react-bootstrap";
import { Link } from "react-router-dom";
import Maberlogo from "../images/Maberlogo.png";
import SimpleReactValidator from "simple-react-validator";
import { loginAction } from "../actions/loginAction";
import { notification } from "antd";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

class Password extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: this.props.location.state.email,
      password: "",
      loading: "false",
      loginError: "",
      loginErrorDisplay: false,
      hidden: true,
    };
    this.toggleShow = this.toggleShow.bind(this);
    this.changeData = this.changeData.bind(this);
    this.submit = this.submit.bind(this);
    this.validator = new SimpleReactValidator();
  }

  changeData(type, event) {
    if (type === "password") {
      this.setState({ password: event.target.value });
    }
  }

  toggleShow() {
    this.setState({ hidden: !this.state.hidden });
  }

  // componentWillMount() {
  //   console.log("Hello Sunil", this.props.location.state.email);
  // }

  componentDidMount() {
    if (this.props.location.state && this.props.location.state.loginError) {
      // this,
      this.setState({
        loginErrorDisplay: true,
        loginError: "Please Login to Access",
      });
    } else {
      // this,
      this.setState({
        loginErrorDisplay: false,
        loginError: "",
      });
    }
  }

  componentWillReceiveProps(postProps) {
    if (!Object.is(postProps.loginReducer, this.props.loginReducer)) {
      if (postProps.loginReducer.data.Error) {
        localStorage.setItem("Email", "");
        notification.open({
          message: "alert",
          description: postProps.loginReducer.data.data.message,
        });
      }
      // console.log("Sunil", postProps.loginReducer);
      if (postProps.loginReducer.data.Loading) {
        this.setState({ loading: true });
      } else {
        this.setState({ loading: false });
      }
    }

    if (
      !postProps.loginReducer.data.Loading &&
      !postProps.loginReducer.data.Error
    ) {
      localStorage.setItem("Email", this.state.email);
      toast.success("LogIn Sucessfully!!!", {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
      // notification.open({
      //   message: "alert",
      //   description: "You are Loggined In",
      // });
      setTimeout(() => {
        this.props.history.push({ pathname: "/Dashboard" });
      }, 5000);
    }
  }

  submit(event) {
    event.preventDefault();
    if (this.validator.allValid()) {
      var self = this;
      this.props.loginAction(this.state.email, this.state.password);
      // notification.open({
      //   message: "alert",
      //   description: "You are successfully Log In Up",
      // });
      // setTimeout(() => {
      //   this.props.history.push({ pathname: "/status" });
      // }, 5000);
    } else {
      this.validator.showMessages();
      this.forceUpdate();
    }
  }

  render() {
    return (
      <Form>
        <div className="site-card-wrapper container">
          <Row>
            <Col className="col-sm-12">
              <Card className="card border-0">
                <div className="col-auto container">
                  <Image
                    style={{
                      width: "50%",
                      marginLeft: "18%",
                      marginTop: "100%",
                    }}
                    src={Maberlogo}
                  />
                </div>

                <div className="col-auto container">
                  <label
                    className="label_Create_Account"
                    style={{ marginRight: "40px" }}
                  >
                    Welcome
                  </label>
                </div>

                <div className="col-auto container">
                  <label
                    style={{ marginRight: "40px", color: "rgba(0, 0, 0, 1)" }}
                    className="lbl"
                  >
                    {this.props.location.state.email}
                  </label>
                </div>

                <div className="col-auto container">
                  <div className="input-group-prepend">
                    <input
                      style={{ width: "600px" }}
                      type={this.state.hidden ? "password" : "text"}
                      className="form-control"
                      placeholder="Password"
                      onChange={this.changeData.bind(this, "password")}
                    />
                  </div>
                  {this.validator.message(
                    "Password",
                    this.state.password,
                    "required",
                    "text-danger"
                  )}
                </div>

                <div className="col-auto container">
                  <div className="input-group-prepend">
                    <input
                      style={{
                        width: "25px",
                        height: "28px",
                        marginRight: "20px",
                      }}
                      onClick={this.toggleShow}
                      type="checkbox"
                    />
                    <label
                      to={"./forgotpassword"}
                      className="show_password"
                      style={{ marginRight: "15em" }}
                    >
                      Show Password
                    </label>
                  </div>
                </div>

                <div className="col-auto container">
                  <div className="input-group-prepend">
                    <Link
                      to={"./forgotpassword"}
                      className="Forgot_Password"
                      style={{ marginRight: "20em" }}
                    >
                      Forgot Password
                    </Link>
                  </div>
                </div>

                <div className="col-auto container">
                  <button
                    type="submit"
                    className="btnNext"
                    loading={this.state.loading}
                    onClick={this.submit}
                  >
                    Next
                    <ToastContainer />
                  </button>
                </div>
              </Card>
            </Col>
          </Row>
        </div>
      </Form>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    loginAction: (email, password) => {
      dispatch(loginAction(email, password));
    },
  };
};

const mapStateToProps = (state) => {
  return { loginReducer: state.loginReducer };
};

export default connect(mapStateToProps, mapDispatchToProps)(Password);
