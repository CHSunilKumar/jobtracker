import React, { Component } from "react";
import logo from "../images/Maber.png";
import { Link } from "react-router-dom";
import { Form, Image, Navbar, Nav } from "react-bootstrap";
import welcome from "../images/Welcome.png";
import process from "../images/Process.png";
import completed from "../images/Completed.png";
import ovedue from "../images/Overdue.png";
import due from "../images/Due.png";
import search from "../images/Search.png";
import inprogress from "../images/InProgress.png";
import add from "../images/Add.png";
import completedselected from "../images/CompletedSelected.png";
import move from "../images/Move.png";
import { Layout, Drawer } from "antd";
import { dashboardAction } from "../actions/dashboardAction";
import { connect } from "react-redux";
import { notification } from "antd";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const { Header, Sider, Content } = Layout;

export class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoaded: false,
      visible: true,
      placement: "left",
      // items: [],
      // Completed: "",
      // Overdue: "",
      // Due: "",
      // Inprogress: "",
    };
  }
  // state = { visible: true, placement: "left" };

  showDrawer = () => {
    this.setState({
      visible: false,
    });
  };

  // onClose = () => {
  //   this.setState({
  //     visible: false,
  //   });
  // };

  // onChange = (e) => {
  //   this.setState({
  //     placement: e.target.value,
  //   });
  // };

  componentDidMount = () => {
    this.props.dashboardAction();
  };

  componentWillReceiveProps(postProps) {
    if (!Object.is(postProps.dashboardReducer, this.props.dashboardReducer)) {
      if (postProps.dashboardReducer.data.Error) {
        // notification.open({
        //   message: "alert",
        //   description: "Some Internal error",
        // });
        toast.success("Sorry ", {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
      if (postProps.dashboardReducer.data.Loading) {
        this.setState({ loading: true });
      } else {
        this.setState({ loading: false });
      }
    }

    if (
      postProps.dashboardReducer.data.data &&
      postProps.dashboardReducer.data.data.data &&
      postProps.dashboardReducer.data.data.data.column
    ) {
      // notification.open({
      //   message: "alert",
      //   description: "You are successfully Signed Up",
      // });
      // setTimeout(() => {
      //   this.props.history.push({ pathname: "/login" });
      // }, 5000);
    }
  }

  render() {
    const {
      placement,
      visible,
      // items,
      // Completed,
      // Overdue,
      // Due,
      // Inprogress,
    } = this.state;

    // const { Completed } = this.props;
    // console.log(Completed);

    return (
      <Form>
        <Layout>
          <Header>
            <div>
              <Navbar
                collapseOnSelect
                expand="lg"
                className="navbar navbar-expand-md navbar-light bg-light"
              >
                <div>
                  <Image src={move} onClick={this.showDrawer} />
                </div>
                <div className="navbar-brand">
                  <div className="col-auto">
                    <Image src={logo} />
                  </div>
                </div>

                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                  <div className="navbar-nav">
                    <div className="search">
                      <input
                        type="text"
                        style={{ borderRadius: "15px" }}
                        placeholder="Search Maber..."
                      />
                      <button type="submit" className="searchButton">
                        <Image src={search} />
                      </button>
                    </div>
                  </div>

                  <div className="navbar-nav ml-auto">
                    <div className="navbar-brand">
                      <Nav className="nav-item nav-link">
                        <Image className="processimage" src={inprogress} />
                      </Nav>
                    </div>
                    <div className="navbar-brand">
                      <Nav className="nav-item nav-link">
                        <Link to={"./Login"}>
                          <Image className="processimage" src={process} />
                        </Link>
                      </Nav>
                    </div>
                    <div>
                      <button type="button" className="btn_Create_Account">
                        CH Sunil Kumar
                      </button>
                    </div>
                  </div>
                </Navbar.Collapse>
              </Navbar>
            </div>
          </Header>
          <Layout>
            <Sider>
              <Drawer
                // title="Basic Drawer"
                placement={placement}
                closable={false}
                // onClose={this.onClose}
                visible={visible}
                // key={placement}
              >
                <div>
                  <div className="sasidebar">
                    <Link
                      to={"./addjob"}
                      className="addjob"
                      style={{
                        textAlign: "center",
                        borderRadius: "30px",
                      }}
                    >
                      <Image className="addicon" src={add} />
                      ADD JOB
                    </Link>

                    <Link to={"#"} style={{ marginTop: "10%" }}>
                      <Image className="completed" src={completed} />
                      Completed <label></label>
                    </Link>
                    <Link to={"#"} style={{ marginTop: "5%" }}>
                      <Image className="overdue" src={ovedue} />
                      Overdue <label></label>
                    </Link>
                    <Link to={"#"} style={{ marginTop: "5%" }}>
                      <Image className="due" src={due} />
                      Due <label></label>
                    </Link>
                    <Link to={"#"} style={{ marginTop: "5%" }}>
                      <Image className="inprogress" src={inprogress} />
                      In Progress
                    </Link>
                  </div>
                </div>
              </Drawer>
            </Sider>

            <Content style={{ float: "right", width: "100%" }}>
              <div className="text-center">
                <div>
                  <Image
                    className="img-fluid"
                    style={{
                      width: "500px",
                      height: "300px",
                      marginTop: "10%",
                    }}
                    src={welcome}
                  />
                  <p>To Maber</p>
                </div>
              </div>
            </Content>
            <ToastContainer />
          </Layout>
        </Layout>
      </Form>
    );
  }
}

const mapStateToProps = (state) => {
  return { dashboardReducer: state.dashboardReducer };
};

const mapDispatchToProps = (dispatch) => {
  return {
    dashboardAction: () => {
      dispatch(dashboardAction());
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
// export default Dashboard;
