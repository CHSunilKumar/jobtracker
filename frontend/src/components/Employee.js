import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Form, Image, Navbar, Nav, Row, Col, Table } from "react-bootstrap";
import logo from "../images/Maber.png";
import process from "../images/Process.png";
import employees from "../images/Employees.png";
import employeesselected from "../images/EmployeesSelected.png";
import department from "../images/Department.png";
import processcreation from "../images/ProcessCreation.png";
import addbuttonactive from "../images/AddButtonActive.png";
import employeephoto from "../images/AddPhoto.png";
import { notification } from "antd";
import { connect } from "react-redux";
import { viewemployeeAction } from "../actions/viewemployeeAction";

export class Employee extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount = () => {
    this.props.viewemployeeAction();
  };

  componentWillReceiveProps(postProps) {
    if (
      !Object.is(postProps.viewemployeeReducer, this.props.viewemployeeReducer)
    ) {
      if (postProps.viewemployeeReducer.data.Error) {
        // notification.open({
        //   message: "alert",
        //   description: "Some Internal error",
        // });
      }
      if (postProps.viewemployeeReducer.data.Loading) {
        this.setState({ loading: true });
      } else {
        this.setState({ loading: false });
      }
    }

    if (
      postProps.viewemployeeReducer.data.data &&
      postProps.viewemployeeReducer.data.data.data &&
      postProps.viewemployeeReducer.data.data.data.column
    ) {
      // notification.open({
      //   message: "alert",
      //   description: "You are successfully Signed Up",
      // });
      // setTimeout(() => {
      //   this.props.history.push({ pathname: "/login" });
      // }, 5000);
    }
  }

  render() {
    return (
      <Form>
        <div className="site-card-wrapper">
          <div>
            <Navbar
              collapseOnSelect
              expand="lg"
              className="navbar navbar-expand-md navbar-light bg-light"
            >
              <div>
                <Image
                  className="processimage"
                  style={{ margin: "50%" }}
                  src={process}
                />
              </div>

              <div className="navbar-brand">
                <div className="col-auto">
                  <Image style={{ marginLeft: "20%" }} src={logo} />
                </div>
              </div>

              <Navbar.Toggle aria-controls="responsive-navbar-nav" />
              <Navbar.Collapse id="responsive-navbar-nav">
                <div className="navbar-nav ml-auto">
                  <div className="navbar-brand mr-auto">
                    <Nav className="topnav-centered">
                      <label className="topnav-centered">
                        PROCESS CREATION BOARD
                      </label>
                    </Nav>
                  </div>

                  <div className="navbar-brand">
                    <button type="button" className="btn_Create_Account">
                      CH Sunil Kumar
                    </button>
                  </div>
                </div>
              </Navbar.Collapse>
            </Navbar>
          </div>

          <div className="sasidebar" style={{ width: "20%" }}>
            <Link to={"./addmilestone"} style={{ marginTop: "5%" }}>
              <Image
                className="employee"
                style={{ marginLeft: "30%" }}
                src={employees}
              />
              Processes
            </Link>
            <Link
              className="active"
              to={"./addemployee"}
              style={{ marginTop: "5%" }}
            >
              <Image
                className="employee"
                style={{ marginLeft: "30%" }}
                src={employeesselected}
              />
              Employees
            </Link>
            <Link to={"./viewdepartments"} style={{ marginTop: "5%" }}>
              <Image
                className="department"
                style={{ marginLeft: "30%" }}
                src={department}
              />
              Departments
            </Link>
            <div>
              <Image className="processimg" src={processcreation} />
            </div>
          </div>

          <div className="container">
            <div className="input-group">
              <label
                className="dptlabel"
                style={{
                  marginLeft: "20%",
                  marginTop: "2%",
                }}
              >
                EMPLOYEES
              </label>
            </div>

            <div className="col-auto">
              <input
                type="text"
                className="form-control"
                style={{
                  width: "40%",
                  borderRadius: "25px",
                  color: "gray",
                  marginLeft: "20%",
                  marginTop: "1%",
                }}
                placeholder="Filter By Name"
              />

              <Image
                style={{ height: "20px", width: "20px", marginLeft: "2%" }}
                src={addbuttonactive}
              />

              <label style={{ color: "gray", marginLeft: "1%" }}>
                Add Employee
              </label>
            </div>

            <div className="col-auto">
              <Table className="tbl table-hover">
                <thead
                  style={{
                    border: "rgba(95, 99, 104, 1)",
                    borderRadius: "10%",
                  }}
                >
                  <tr style={{ color: "gray", borderRadius: "20px" }}>
                    <th>Name</th>
                    <th>Department</th>
                    <th>Designation</th>
                    <th>Employee Code</th>
                  </tr>
                </thead>
                <tbody style={{ color: "gray" }}>
                  {/* <tr>
                    <td>Sunil Kumar</td>
                    <td>Testing</td>
                    <td>MCA</td>
                    <td>SEP1001</td>
                  </tr>
                  <tr>
                    <td>Sunil Kumar</td>
                    <td>Testing</td>
                    <td>MCA</td>
                    <td>SEP1001</td>
                  </tr> */}
                  <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                </tbody>
              </Table>
            </div>
            <br />
          </div>
        </div>
      </Form>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    viewemployeeAction: (firstname, employeecode, department, designation) => {
      dispatch(
        viewemployeeAction(firstname, employeecode, department, designation)
      );
    },
  };
};

const mapStateToProps = (state) => {
  return { viewemployeeReducer: state.viewemployeeReducer };
};

export default connect(mapStateToProps, mapDispatchToProps)(Employee);
