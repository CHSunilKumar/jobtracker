import React, { Component } from "react";
import { Card, Col, Form, Row, Image } from "react-bootstrap";
import registration from "../images/registration.png";
import logo from "../images/Maber.png";
import { registerAction } from "../actions/registerAction";
import { connect } from "react-redux";
import { notification } from "antd";
import SimpleReactValidator from "simple-react-validator";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

export class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstname: "",
      lastname: "",
      email: "",
      phonenumber: "",
      password: "",
      confirmpassword: "",
      loading: "false",
      signUpError: "",
      signUpDisplay: false,
      passwordNoMatch: false,
      hidden: true,
    };
    this.toggleShow = this.toggleShow.bind(this);
    this.changeData = this.changeData.bind(this);
    this.validator = new SimpleReactValidator();
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  changeData(type, event) {
    switch (type) {
      case "firstname":
        this.setState({ firstname: event.target.value });
        break;
      case "lastname":
        this.setState({ lastname: event.target.value });
        break;
      case "email":
        this.setState({ email: event.target.value });
        break;
      case "phonenumber":
        this.setState({ phonenumber: event.target.value });
        break;
      case "password":
        this.setState({ password: event.target.value });
        break;
      case "confirmpassword":
        this.setState({ confirmpassword: event.target.value });
        break;
      default:
        break;
    }

    if (type === "confirmpassword") {
      this.setState({ confirmpassword: event.target.value }, () => {
        if (this.state.password === this.state.confirmpassword) {
          this.setState({ passwordNoMatch: false });
        } else {
          this.setState({ passwordNoMatch: true });
        }
      });
    }
  }

  toggleShow() {
    this.setState({ hidden: !this.state.hidden });
  }

  componentWillReceiveProps(postProps) {
    if (!Object.is(postProps.registerReducer, this.props.registerReducer)) {
      if (postProps.registerReducer.data.Error) {
        notification.open({
          message: "alert",
          description: postProps.registerReducer.data.data.error.message,
        });
      }
      if (postProps.registerReducer.data.Loading) {
        this.setState({ loading: true });
      } else {
        this.setState({ loading: false });
      }
    }

    if (
      !postProps.registerReducer.data.Loading &&
      !postProps.registerReducer.data.Error
    ) {
      notification.open({
        message: "alert",
        description: "You are successfully Signed Up",
      });
      setTimeout(() => {
        this.props.history.push({ pathname: "/login" });
      }, 5000);
    }
  }

  handleSubmit(event) {
    event.preventDefault();
    let passwordMatch = true;
    if (this.state.password === this.state.confirmPassword) {
      this.setState({ passwordNoMatch: true });
      passwordMatch = false;
    }

    if (this.validator.allValid() && passwordMatch) {
      var self = this;

      toast.success("User Registered Sucessfully", {
        position: "top-right",
        autoClose: 10000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
      // notification.open({
      //   message: "alert",
      //   description: "You are successfully Signed Up",
      // });
      setTimeout(() => {
        this.props.history.push({ pathname: "/login" });
      }, 5000);
      this.props.registerAction(
        this.state.firstname,
        this.state.lastname,
        this.state.email,
        this.state.phonenumber,
        this.state.password,
        this.state.confirmpassword
      );
    } else {
      this.validator.showMessages();
      this.forceUpdate();
    }
  }

  render() {
    return (
      <Form>
        <div className="site-card-wrapper container">
          <span>{this.state.signUpError}</span>
          <center>
            <label style={{ display: this.state.signUpDisplay }}>
              {this.state.signUpError}
            </label>
          </center>
          <Row>
            <Col className="col-sm-6">
              <Card className="card border-0">
                <div className="input-group">
                  <div className="col-auto input-group-prepend">
                    <Image className="logo" src={logo} />
                  </div>
                </div>
                <div className="input-group">
                  <div className="col-auto input-group-prepend">
                    <label className="label_Create_Account">
                      Create your Maber Account
                    </label>
                  </div>
                </div>
                <div className="input-group">
                  <div className="col-auto input-group-prepend">
                    <Row>
                      <Col>
                        <input
                          style={{ width: "100%" }}
                          type="text"
                          className="form-control"
                          placeholder="First Name"
                          onChange={this.changeData.bind(this, "firstname")}
                        />
                        {this.validator.message(
                          "First Name",
                          this.state.firstname,
                          "required|alpha|min:3|max:20",
                          "text-danger"
                        )}
                      </Col>
                      <Col>
                        <input
                          style={{ width: "100%" }}
                          type="text"
                          className="form-control"
                          placeholder="Last Name"
                          onChange={this.changeData.bind(this, "lastname")}
                        />
                        {this.validator.message(
                          "Last Name",
                          this.state.lastname,
                          "required|alpha|min:3|max:20",
                          "text-danger"
                        )}
                      </Col>
                    </Row>
                  </div>
                </div>
                <div className="input-group">
                  <div className="col-auto input-group-prepend">
                    <input
                      style={{ width: "615px" }}
                      type="email"
                      className="form-control"
                      placeholder="Email Address"
                      onChange={this.changeData.bind(this, "email")}
                    />
                  </div>
                  {this.validator.message(
                    "Email",
                    this.state.email,
                    "required|email",
                    "text-danger"
                  )}
                </div>

                <div className="input-group">
                  <div className="col-auto input-group-prepend">
                    <select
                      style={{
                        width: "90px",
                        height: "45px",
                        textAlignLast: "center",
                      }}
                      className="ddl form-control"
                      defaultValue="India"
                    >
                      <option value="India">+91</option>
                      <option value="Amercia">+72</option>
                    </select>
                    <input
                      type="tel"
                      style={{ width: "520px" }}
                      className="col-sm-10 form-control"
                      placeholder="Mobile Number"
                      onChange={this.changeData.bind(this, "phonenumber")}
                    />
                  </div>
                  {this.validator.message(
                    "Mobile Number",
                    this.state.phonenumber,
                    "required|numeric|min:10",
                    "text-danger"
                  )}
                </div>
                <div className="input-group">
                  <div className="col-auto input-group-prepend">
                    <Row>
                      <Col>
                        <input
                          style={{ width: "100%" }}
                          type={this.state.hidden ? "password" : "text"}
                          className="form-control"
                          placeholder="Password"
                          onChange={this.changeData.bind(this, "password")}
                        />
                        {this.validator.message(
                          "Password",
                          this.state.password,
                          "required",
                          "text-danger"
                        )}
                      </Col>
                      <Col>
                        <input
                          style={{ width: "100%" }}
                          type={this.state.hidden ? "password" : "text"}
                          className="form-control"
                          placeholder="Confirm"
                          onChange={this.changeData.bind(
                            this,
                            "confirmpassword"
                          )}
                        />
                        {this.validator.message(
                          "Confirm Password",
                          this.state.confirmpassword,
                          "required",
                          "text-danger"
                        )}
                        {this.state.passwordNoMatch && (
                          <center>
                            <span style={{ color: "red" }}>
                              Passwords Should Match
                            </span>
                          </center>
                        )}
                      </Col>
                    </Row>
                  </div>
                </div>
                <div className="input-group">
                  <div className="col-auto input-group-prepend">
                    <h6 className="lbl">
                      Use 8 or more characters with a mix of letters, numbers
                      and symbols
                    </h6>
                  </div>
                </div>
                <div className="input-group">
                  <div className="col-auto continer">
                    <input
                      style={{ width: "25px", height: "28px" }}
                      onClick={this.toggleShow}
                      type="checkbox"
                    />
                    <label
                      className="show_password"
                      style={{ marginLeft: "20px" }}
                    >
                      Show Password
                    </label>
                  </div>
                </div>

                {/* <div className="input-group">
                  <div className="col-auto input-group-prepend">
                    <input
                      style={{ width: "615px" }}
                      type="email"
                      className="form-control"
                      placeholder="Key number"
                    />
                  </div>
                </div> */}

                <div className="input-group">
                  <div className="col-auto input-group-prepend">
                    <button
                      type="submit"
                      className="btn_reg"
                      loading={this.state.loading}
                      onClick={this.handleSubmit}
                    >
                      Sign Up
                    </button>
                    <ToastContainer />
                  </div>
                </div>
              </Card>
            </Col>
            <Col className="col-sm-6">
              <Card className="imgcard border-0">
                <div className="input-group">
                  <div className="col-auto input-group-prepend">
                    <Image
                      className="img-responsive"
                      style={{
                        height: "100%",
                        width: "100%",
                        marginLeft: "25%",
                      }}
                      src={registration}
                    />
                  </div>
                </div>
              </Card>
            </Col>
          </Row>
        </div>
      </Form>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    registerAction: (
      firstname,
      lastname,
      email,
      phonenumber,
      password,
      confirmpassword
    ) => {
      dispatch(
        registerAction(
          firstname,
          lastname,
          email,
          phonenumber,
          password,
          confirmpassword
        )
      );
    },
  };
};

const mapStateToProps = (state) => {
  return { registerReducer: state.registerReducer };
};

export default connect(mapStateToProps, mapDispatchToProps)(Register);
