import React, { Component } from "react";
import logo from "../images/Maber.png";
import { Link } from "react-router-dom";
import { Form, Image, Navbar, Nav } from "react-bootstrap";

export class Home extends Component {
  render() {
    return (
      <Form>
        <div>
          <Navbar
            collapseOnSelect
            expand="lg"
            className="navbar navbar-expand-md navbar-light bg-light"
          >
            <div className="navbar-brand">
              <div className="col-auto">
                <Image className="" src={logo} />
              </div>
            </div>
            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
            <Navbar.Collapse id="responsive-navbar-nav">
              {/* <div className="collapse navbar-collapse"> */}
              <div className="navbar-nav ml-auto">
                <Nav className="nav-item nav-link">
                  <Link
                    to={"./Login"}
                    style={{
                      fontSize: "20px",
                      marginRight: "20px",
                      color: "black",
                    }}
                  >
                    Sign in
                  </Link>
                </Nav>
              </div>
              {/* </div> */}
              <button
                type="button"
                onClick={() => {
                  this.props.history.push("/register");
                }}
                className="btn_Create_Account"
              >
                Create an account
              </button>
            </Navbar.Collapse>
          </Navbar>
        </div>
      </Form>
    );
  }
}

export default Home;
