import { createStore, combineReducers, applyMiddleware } from "redux";
import loginReducer from "./reducers/loginReducer";
import dashboardReducer from "./reducers/dashboardReducer";
import registerReducer from "./reducers/registerReducer";
import adddepartmentReducer from "./reducers/adddepartmentReducer";
import getuserReducer from "./reducers/getuserReducer";
import forgotpasswordReducer from "./reducers/forgotpasswordReducer";
import addemployeeReducer from "./reducers/addemployeeReducer";
import viewemployeeReducer from "./reducers/viewemployeeReducer";
import thunk from "redux-thunk";

const reducer = combineReducers({
  registerReducer,
  loginReducer,
  dashboardReducer,
  adddepartmentReducer,
  getuserReducer,
  forgotpasswordReducer,
  addemployeeReducer,
  viewemployeeReducer,
});

const store = createStore(reducer, applyMiddleware(thunk));
export default store;
