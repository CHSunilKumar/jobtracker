import { FORGOTPASSWORDSUCCESS, FORGOTPASSWORDLOADING, FORGOTPASSWORDERROR } from "../types";
import axios from "axios";
import { forgotPasswordURL } from "../baseConfig";
const headers = {
  "Content-Type": "application/json",
};

export const forgotpasswordAction = (
  Email
) => {
  let data = {
    Email: Email
  };
  return (dispatch) => {
    dispatch({
      type: FORGOTPASSWORDLOADING,
    });
    axios
      .post(forgotPasswordURL, data, {
        headers: headers,
      })
      .then((response) => {
        dispatch({
          type: FORGOTPASSWORDSUCCESS,
          payload: {
            response,
          },
        });
      })
      .catch((error) => {
        dispatch({
          type: FORGOTPASSWORDERROR,
          payload: {
            error,
          },
        });
      });
  };
};

export default forgotpasswordAction;
