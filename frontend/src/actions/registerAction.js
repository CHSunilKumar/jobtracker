import { SIGNUPSUCCESS, SIGNUPLOADING, SIGNUPERROR } from "../types";
import axios from "axios";
import { registerURL } from "../baseConfig";
const headers = {
  "Content-Type": "application/json",
};

export const registerAction = (
  firstname,
  lastname,
  email,
  phonenumber,
  password,
  confirmpassword
) => {
  let data = {
    firstname: firstname,
    lastname: lastname,
    email: email,
    phonenumber: phonenumber,
    password: password,
    confirmpassword: confirmpassword,
  };
  return (dispatch) => {
    dispatch({
      type: SIGNUPLOADING,
    });
    axios
      .post(registerURL, data, {
        headers: headers,
      })
      .then((response) => {
        dispatch({
          type: SIGNUPSUCCESS,
          payload: {
            response,
          },
        });
      })
      .catch((error) => {
        dispatch({
          type: SIGNUPERROR,
          payload: {
            error,
          },
        });
      });
  };
};
