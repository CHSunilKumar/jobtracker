import { ADDDEPARTMENTSUCCESS, ADDDEPARTMENTLOADING, ADDDEPARTMENTERROR } from "../types";
import axios from "axios";
import { adddepartmentURL } from "../baseConfig";
const headers = {
  "Content-Type": "application/json",
};

export const adddepartmentAction = (
    Department_id,
    Department_Name,
    Department_Members,
    Department_Supervisor
) => {
  let data = {
    Department_id:  Department_id,
    Department_Name: Department_Name,
    Department_Members: Department_Members,
    Department_Supervisor: Department_Supervisor
  };
  return (dispatch) => {
    dispatch({
      type: ADDDEPARTMENTLOADING,
    });
    axios
      .post(adddepartmentURL, data, {
        headers: headers,
      })
      .then((response) => {
        dispatch({
          type: ADDDEPARTMENTSUCCESS,
          payload: {
            response,
          },
        });
      })
      .catch((error) => {
        dispatch({
          type: ADDDEPARTMENTERROR,
          payload: {
            error,
          },
        });
      });
  };
};

export default adddepartmentAction;
