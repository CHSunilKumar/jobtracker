import { ADDEMPLOYEESUCCESS,ADDEMPLOYEELOADING ,ADDEMPLOYEEERROR  } from "../types";
import axios from "axios";
import { addemployeeURL } from "../baseConfig";
const headers = {
  "Content-Type": "application/json",
};

export const addemployeeAction = (
    firstname,
    lastname,
    employeecode,
    department,
    supervisor,
    designation,
    selectedrole,
    officetel,
    mobile,
    email,
    officename
) => {
  let data = {
    firstname:firstname,
    lastname:lastname,
    employeecode:employeecode,
    department:department,
    supervisor:supervisor,
    designation:designation,
    selectedrole:selectedrole,
    officetel:officetel,
    mobile:mobile,
    email:email,
    officename:officename,
  };
  return (dispatch) => {
    dispatch({
      type: ADDEMPLOYEELOADING,
    });
    axios
      .post(addemployeeURL, data, {
        headers: headers,
      })
      .then((response) => {
        dispatch({
          type: ADDEMPLOYEESUCCESS,
          payload: {
            response,
          },
        });
      })
      .catch((error) => {
        dispatch({
          type: ADDEMPLOYEEERROR,
          payload: {
            error,
          },
        });
      });
  };
};