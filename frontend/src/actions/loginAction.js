import {
  LOGINSUCCESS,
  LOGINLOADING,
  LOGINERROR,
  LOGINNEWPASSWORD,
} from "../types";
import { loginURL } from "../baseConfig";
import axios from "axios";
const headers = {
  "Content-Type": "application/json",
};

export const loginAction = (email, password) => {
  let data = { email: email, password: password };
  return (dispatch) => {
    dispatch({
      type: LOGINLOADING,
    });
    axios
      .post(loginURL, data, {
        headers: headers,
      })
      .then((response) => {
        dispatch({
          type: LOGINSUCCESS,
          payload: {
            response,
          },
        });
      })
      .catch((error) => {
        dispatch({
          type: LOGINERROR,
          payload: {
            error,
          },
        });
      });
  };
};
// const user = new CognitoUser({
//   Username: userName,
//   Pool: userPool,
// });
// const authDetails = new AuthenticationDetails({
//   Username: userName,
//   Password: password,
// });
// user.authenticateUser(authDetails, {
//   onSuccess: (data) => {
//     Analytics.record("userLoginEvent");
//     Analytics.record("userSignin", { username: userName });
//     dispatch({
//       type: LOGINSUCCESS,
//       payload: {
//         data,
//       },
//     });
//   },
//   onFailure: (error) => {
//     dispatch({
//       type: LOGINERROR,
//       payload: {
//         error,
//       },
//     });
//   },
// });
