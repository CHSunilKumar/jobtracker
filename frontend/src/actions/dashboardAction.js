import {
  DASHBOARDDATASUCCESS,
  DASHBOARDDATALOADING,
  DASHBOARDDATAERROR,
} from "../types";
import { dashboardURL } from "../baseConfig";
import axios from "axios";
const headers = {
  "Content-Type": "application/json",
};

export const dashboardAction = () => {
  //   let data = { Completed, Overdue, Due, Inprogress };
  //   let data = { summaryReportDate: summaryReportDate };
  //   const endoint = baseSummaryURL + "reportDate=" + summaryReportDate;
  return async (dispatch) => {
    dispatch({
      type: DASHBOARDDATALOADING,
    });
    axios
      .get(dashboardURL, {
        headers: headers,
      })
      .then((response) => {
        dispatch({
          type: DASHBOARDDATASUCCESS,
          payload: response.data,
        });
      })
      .catch((error) => {
        dispatch({
          type: DASHBOARDDATAERROR,
          payload: {
            error,
          },
        });
      });
  };
};
