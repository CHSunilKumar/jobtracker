import { VIEWEMPLOYEESUCCESS, VIEWEMPLOYEELOADING, VIEWEMPLOYEEERROR } from "../types";
import axios from "axios";
import { viewemployeeURL } from "../baseConfig";
const headers = {
  "Content-Type": "application/json",
};

export const viewemployeeAction = (
    _id,
    firstname,
    department,
    designation,
    employeecode
) => {
  let data = {
    _id:  _id,
    firstname: firstname,
    department:department,
    designation:designation,
    employeecode:employeecode
  };
  return dispatch => {
    dispatch({ 
      type: VIEWEMPLOYEELOADING
  });
  axios.get(viewemployeeURL, data, {
    headers: headers,
  })
  .then((response) => {
    console.log("viewemployeeAction",response)
    dispatch({
      type: VIEWEMPLOYEESUCCESS,
      payload: {
        response ,
      },
      
    })
  })
  .catch((error) => {
    dispatch({
      type: VIEWEMPLOYEEERROR,
      payload: {
        error,
      },
    });
  });
};
};

export default viewemployeeAction;
