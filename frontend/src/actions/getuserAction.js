import { GETUSERSUCCESS, GETUSERLOADING, GETUSERERROR } from "../types";
import axios from "axios";
import { getuserURL } from "../baseConfig";
const headers = {
  "Content-Type": "application/json",
};

export const getuserAction = (
    _id,
    firstname
) => {
  let data = {
    _id:  _id,
    firstname: firstname
  };
  return dispatch => {
    dispatch({ 
      type: GETUSERLOADING
  });
  axios.get(getuserURL, data, {
    headers: headers,
  })
  .then((response) => {
    console.log("getuserAction",response)
    dispatch({
      type: GETUSERSUCCESS,
      payload: {
        response ,
      },
      
    })
  })
  .catch((error) => {
    dispatch({
      type: GETUSERERROR,
      payload: {
        error,
      },
    });
  });
};
};

export default getuserAction;
