import React from "react";
import "./App.css";
import {
  BrowserRouter as Router,
  HashRouter,
  Route,
  Switch,
} from "react-router-dom";
import Home from "./components/Home";
import Register from "./components/Register";
import Login from "./components/Login";
import Password from "./components/Password";
import Dashboard from "./components/Dashboard";
import ForgotPassword from "./components/ForgotPassword";
import AddDepartment from "./components/AddDepartment";
import AddEmployee from "./components/AddEmployee";
import Employee from "./components/Employee";
import "bootstrap/dist/css/bootstrap.min.css";
// import "antd/dist/antd.css";

export default () => {
  return (
    <Router>
      <Switch>
        <Route path="/" component={Home} exact />
        <Route path="/register" component={Register} exact />
        <Route path="/login" component={Login} exact />
        <Route path="/password" component={Password} exact />
        <Route path="/forgotpassword" component={ForgotPassword} exact />
        <Route path="/dashboard" component={Dashboard} exact />
        <Route path="/adddepartment" component={AddDepartment} exact />
        <Route path="/addemployee" component={AddEmployee} exact />
        <Route path="/employee" component={Employee} exact />
      </Switch>
    </Router>
  );
};
