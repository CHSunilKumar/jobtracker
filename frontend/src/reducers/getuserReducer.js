import { GETUSERSUCCESS, GETUSERLOADING, GETUSERERROR } from "../types";
const initState = {
  data: {
    data: {},
    Loading: false,
    Error: false,
  },
};

export default (state = initState, action) => {
  console.log("getuserReducer",action.payload)
  switch (action.type) {
    case GETUSERLOADING:
      return {
        ...state,
        data: {
          data: {},
          Loadind: true,
          error: false,
        },
      };

    case GETUSERSUCCESS:
      console.log("getuserReducer",action.payload)
      return {
        ...state,
        data: {
          data: action.payload,
          Loading: false,
          Error: false,
        },
      };

    case GETUSERERROR:
      return {
        ...state,
        data: {
          data: action.payload,
          Loading: false,
          Error: true,
        },
      };

    default:
      return state;
  }
};
