import { SIGNUPSUCCESS, SIGNUPLOADING, SIGNUPERROR } from "../types";
const initState = {
  data: {
    data: {},
    Loading: false,
    Error: false,
  },
};

export default (state = initState, action) => {
  switch (action.type) {
    case SIGNUPLOADING:
      return {
        ...state,
        data: {
          data: {},
          Loadind: true,
          Error: false,
        },
      };

    case SIGNUPSUCCESS:
      return {
        ...state,
        data: {
          data: action.payload,
          Loading: false,
          Error: false,
        },
      };

    case SIGNUPERROR:
      return {
        ...state,
        data: {
          data: action.payload,
          Loading: false,
          Error: true,
        },
      };

    default:
      return state;
  }
};
