import { FORGOTPASSWORDSUCCESS, FORGOTPASSWORDLOADING, FORGOTPASSWORDERROR } from "../types";
const initState = {
  data: {
    data: {},
    Loading: false,
    Error: false,
  },
};

export default (state = initState, action) => {
  switch (action.type) {
    case FORGOTPASSWORDLOADING:
      return {
        ...state,
        data: {
          data: {},
          Loadind: true,
          error: false,
        },
      };

    case FORGOTPASSWORDSUCCESS:
      return {
        ...state,
        data: {
          data: action.payload,
          Loading: false,
          Error: false,
        },
      };

    case FORGOTPASSWORDERROR:
      return {
        ...state,
        data: {
          data: action.payload,
          Loading: false,
          Error: true,
        },
      };

    default:
      return state;
  }
};
