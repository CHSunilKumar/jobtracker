import { ADDEMPLOYEESUCCESS,ADDEMPLOYEELOADING ,ADDEMPLOYEEERROR } from "../types";
const initState = {
  data: {
    data: {},
    Loading: false,
    Error: false,
  },
};

export default (state = initState, action) => {
  switch (action.type) {
    case ADDEMPLOYEELOADING:
      return {
        ...state,
        data: {
          data: {},
          Loadind: true,
          Error: false,
        },
      };

    case ADDEMPLOYEESUCCESS:
      return {
        ...state,
        data: {
          data: action.payload,
          Loading: false,
          Error: false,
        },
      };

    case ADDEMPLOYEEERROR:
      return {
        ...state,
        data: {
          data: action.payload,
          Loading: false,
          Error: true,
        },
      };

    default:
      return state;
  }
};