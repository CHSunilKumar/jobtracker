import { ADDDEPARTMENTSUCCESS, ADDDEPARTMENTLOADING, ADDDEPARTMENTERROR } from "../types";
const initState = {
  data: {
    data: {},
    Loading: false,
    Error: false,
  },
};

export default (state = initState, action) => {
  switch (action.type) {
    case ADDDEPARTMENTLOADING:
      return {
        ...state,
        data: {
          data: {},
          Loadind: true,
          error: false,
        },
      };

    case ADDDEPARTMENTSUCCESS:
      return {
        ...state,
        data: {
          data: action.payload,
          Loading: false,
          Error: false,
        },
      };

    case ADDDEPARTMENTERROR:
      return {
        ...state,
        data: {
          data: action.payload,
          Loading: false,
          Error: true,
        },
      };

    default:
      return state;
  }
};
