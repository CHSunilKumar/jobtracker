import { LOGINSUCCESS, LOGINLOADING, LOGINERROR } from "../types";
const initState = {
  data: {
    data: {},
    Loading: false,
    Error: false,
    // newPassword: false,
  },
};
export default (state = initState, action) => {
  switch (action.type) {
    case LOGINLOADING:
      return {
        ...state,
        data: {
          data: {},
          Loading: true,
          Error: false,
          //   newPassword: false,
        },
      };
    case LOGINSUCCESS:
      return {
        ...state,
        data: {
          data: action.payload,
          Loading: false,
          Error: false,
          //   newPassword: false,
        },
      };

    case LOGINERROR:
      return {
        ...state,
        data: {
          data: action.payload,
          Loading: false,
          Error: true,
          //   newPassword: false,
        },
      };
    default:
      return state;
  }
};
