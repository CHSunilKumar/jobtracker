import { VIEWEMPLOYEESUCCESS, VIEWEMPLOYEELOADING, VIEWEMPLOYEEERROR } from "../types";
const initState = {
  data: {
    data: {},
    Loading: false,
    Error: false,
  },
};

export default (state = initState, action) => {
  console.log("viewemployeeReducer",action.payload)
  switch (action.type) {
    case VIEWEMPLOYEELOADING:
      return {
        ...state,
        data: {
          data: {},
          Loadind: true,
          error: false,
        },
      };

    case VIEWEMPLOYEESUCCESS:
      console.log("viewemployeeReducer",action.payload)
      return {
        ...state,
        data: {
          data: action.payload,
          Loading: false,
          Error: false,
        },
      };

    case VIEWEMPLOYEEERROR:
      return {
        ...state,
        data: {
          data: action.payload,
          Loading: false,
          Error: true,
        },
      };

    default:
      return state;
  }
};
