import {
  DASHBOARDDATASUCCESS,
  DASHBOARDDATALOADING,
  DASHBOARDDATAERROR,
} from "../types";
const initState = {
  data: {
    data: [],
    Loading: false,
    Error: false,
  },
};
export default (state = initState, action) => {
  switch (action.type) {
    case DASHBOARDDATALOADING:
      return {
        ...state,
        data: {
          data: {},
          Loading: true,
          Error: false,
        },
      };
    case DASHBOARDDATASUCCESS:
      return {
        ...state,
        data: {
          data: action.payload.response,
          Loading: false,
          Error: false,
        },
      };

    case DASHBOARDDATAERROR:
      return {
        ...state,
        data: {
          data: action.payload,
          Loading: false,
          Error: true,
        },
      };
    default:
      return state;
  }
};
